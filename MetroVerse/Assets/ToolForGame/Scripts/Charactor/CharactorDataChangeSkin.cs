﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class CharactorDataChangeSkin : MonoBehaviour
{
    [SerializeField]
    private CharactorDataGeneratorInfomationConvertToSprite _charactorDataGeneratorInfomationConvertToSprite;


    [SerializeField] private CharactorDataGeneratorInfomation[] _charactorDataGeneratorInfomation;

    private CharactorSpriteController[] _charactorSpriteControllers;

    private CharactorSpriteController[] CharactorSpriteControllers
    {
        get
        {
            if (this._charactorSpriteControllers == null)
            {
                this._charactorSpriteControllers = this.GetComponentsInChildren<CharactorSpriteController>();
            }

            return this._charactorSpriteControllers;
        }
    }

    private bool inited = false;

    public void SetOnStart(CharactorDataGeneratorInfomation charactorDataGeneratorInfomation)
    {
        this.inited = true;
    }

    [Button]
    public void LoadBoySpriteData(ESKINCOLOR eskincolor)
    {
        switch (eskincolor)
        {
            case ESKINCOLOR.WHITE:
                break;
            case ESKINCOLOR.YELLOW:
                break;
            case ESKINCOLOR.BLACK:
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(eskincolor), eskincolor, null);
        }
    }

    public void LoadAllDataSprite(CharactorDataGeneratorInfomation charactorDataGeneratorInfomation)
    {
        this._charactorDataGeneratorInfomationConvertToSprite.Etypecharactor = charactorDataGeneratorInfomation.Etypecharactor;
    }
}