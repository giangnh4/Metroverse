﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIEditorCpnController : MonoBehaviour
{
    [SerializeField] private ItemsControllers[] _itemsControllersArray;
    
}

[System.Serializable]
public class ItemsControllers
{
    public ESKINCHARACTORCPN Eskincharactorcpn;
    public Button btnButton;
    public Transform ParentPanel;
}