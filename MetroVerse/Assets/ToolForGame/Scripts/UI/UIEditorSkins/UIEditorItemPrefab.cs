﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIEditorItemPrefab : MonoBehaviour
{
    [SerializeField] private ItemStyle _itemStyle;
    [SerializeField] private Image ImgChoosed;
    [SerializeField] private Image _imageIcon;
    private bool inited = false;

    public void SetOnStart(ItemStyle itemStyle)
    {
        this._itemStyle = itemStyle;
        this._imageIcon.sprite = this._itemStyle.Sprite;
        this._imageIcon.SetNativeSize();
        this.inited = true;
    }

    public void IsOnChoose(bool isChoosed)
    {
        this.ImgChoosed.gameObject.SetActive(isChoosed);
    }
}