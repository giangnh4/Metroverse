﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Game.Data.DataScriptObjectsAble;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

public class DataCharactorManagers : MonoBehaviour
{
    #region Singleton

    private static DataCharactorManagers instance;

    public static DataCharactorManagers Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<DataCharactorManagers>();
                DontDestroyOnLoad(instance);
            }

            return instance;
        }
    }

    #endregion

    /// <summary>
    /// All Charactor Genertor Data
    /// </summary>
    [FormerlySerializedAs("CharactorDatas")]
    public DataScriptObjectAble[] charactorDatas;

    /// <summary>
    /// this is Func Load Sprite by ID
    /// </summary>
    /// <param name="etypecharactor"> Type Charator. Ex:BOY ,GIRL , Special</param>
    /// <param name="echaractordetail"> Type Charator details. Ex : body ,head, hair ....</param>
    /// <param name="id"> ID Sprite Needed</param>
    /// <returns></returns>
    public Sprite LoadSpiteByID(ETYPECHARACTOR etypecharactor, ECHARACTORDETAIL echaractordetail, string id)
    {
        Sprite sprite = null;
        foreach (var item in charactorDatas)
        {
            if (item.Etypecharactor.Equals(etypecharactor))
            {
                var itemStyles = GetItemStyles(echaractordetail, item);
                foreach (var itemStyle in itemStyles.Where(itemStyle => itemStyle.ID.Equals(id)))
                {
                    sprite = itemStyle.Sprite;
                    break;
                }

                return sprite;
            }
        }

        Debug.Log("Body Skin not found !");
        return null;
    }


    public Sprite[] LoadSpriteAnimationByID(ETYPECHARACTOR etypecharactor, ECHARACTORDETAIL echaractordetail, string id)
    {
        Sprite[] sprites = null;
        foreach (var item in this.charactorDatas)
        {
            if (item.Etypecharactor.Equals(etypecharactor))
            {
                var itemStyle = GetItemStyles(echaractordetail, item);
                foreach (var style in itemStyle)
                {
                    if (style.ID.Equals(id))
                    {
                        sprites = new Sprite[style._itemsItemAnimations.Count];
                        for (int i = 0; i < style._itemsItemAnimations.Count; i++)
                        {
                            sprites[i] = style._itemsItemAnimations[i].Sprite;
                        }

                        return sprites;
                    }
                }
            }
        }

        Debug.Log("Sprite Animation Not Found ! " + id);
        return sprites;
    }

    #region staticFunc

    private static List<ItemStyle> GetItemStyles(ECHARACTORDETAIL echaractordetail, DataScriptObjectAble item)
    {
        List<ItemStyle> itemStyles;
        switch (echaractordetail)
        {
            case ECHARACTORDETAIL.BODY:
                itemStyles = item.bodyItemStyles;
                break;
            case ECHARACTORDETAIL.Face:
                itemStyles = item.FacesItemStyles;
                break;
            case ECHARACTORDETAIL.HAIR:
                itemStyles = item.hairItemStyles;
                break;
            case ECHARACTORDETAIL.HAIRLONG:
                itemStyles = item.hairLongItemStyles;
                break;
            case ECHARACTORDETAIL.TOP:
                itemStyles = item.topItemStyles;
                break;
            case ECHARACTORDETAIL.BOTTOM:
                itemStyles = item.bottomItemStyles;
                break;
            case ECHARACTORDETAIL.ACCESSOIRES:
                itemStyles = item.AccessoiresItemStyles;
                break;
            case ECHARACTORDETAIL.VEHICLE:
                itemStyles = item.vehicleItemStyles;
                break;
            default:
                itemStyles = null;
                break;
        }

        return itemStyles;
    }

    #endregion
}