﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

namespace Game.Data.DataScriptObjectsAble
{
    [CreateAssetMenu(fileName = "DataGlobal", menuName = "MyGame/DataGlobal")]
    public class DataScriptObjectAble : ScriptableObject
    {
        public ETYPECHARACTOR Etypecharactor = ETYPECHARACTOR.BOY;
        [BoxGroup("Body Skins")] public List<ItemStyle> bodyItemStyles = new List<ItemStyle>();
        // [BoxGroup("Head Skins")] public List<ItemStyle> headItemStyles = new List<ItemStyle>();
        [FormerlySerializedAs("eyeItemStyles")] [BoxGroup("Face Skins")] public List<ItemStyle> FacesItemStyles = new List<ItemStyle>();
        [BoxGroup("Hair Skins")] public List<ItemStyle> hairItemStyles = new List<ItemStyle>();
        [BoxGroup("HairLong Skins")] public List<ItemStyle> hairLongItemStyles = new List<ItemStyle>();

        [FormerlySerializedAs("upItemStyles")] [BoxGroup("Top Skins")]
        public List<ItemStyle> topItemStyles = new List<ItemStyle>();

        [FormerlySerializedAs("downItemStyles")] [BoxGroup("Bottom Skins")]
        public List<ItemStyle> bottomItemStyles = new List<ItemStyle>();

        [FormerlySerializedAs("accessoiItemStyles")] [BoxGroup("Accessoires Skins")] public List<ItemStyle> AccessoiresItemStyles = new List<ItemStyle>();
        [BoxGroup("Vehicle Skins")] public List<ItemStyle> vehicleItemStyles = new List<ItemStyle>();
    }
}

[System.Serializable]
public class DataCharactorGenerate
{
    private List<ItemStyle> _itemStyles = new List<ItemStyle>();
}

[System.Serializable]
public class ItemStyle
{
    [LabelWidth(100)] public string ID;


    [HorizontalGroup("Item datas")] [PreviewField(75)] [HideLabel]
    public Sprite Sprite;

    [FormerlySerializedAs("_bodyAnimations")] [VerticalGroup("Item datas/Animation")]
    
    public List<ItemAnimation> _itemsItemAnimations = new List<ItemAnimation>();
}

[System.Serializable]
public class ItemAnimation
{
    [LabelWidth(100)] public string ID;
    [PreviewField(75)] public Sprite Sprite;

    public ItemAnimation()
    {
    }

    public ItemAnimation(string _id = "", Sprite _sprite = null)
    {
        this.ID = _id;
        this.Sprite = _sprite;
    }
}