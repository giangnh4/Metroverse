﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharactorMovement : MonoBehaviour
{
    Rigidbody2D body;
    private Vector2 move = Vector2.zero;
    public float runSpeed = 10.0f;
    private bool m_FacingRight = true;
    private bool moving = false;
    private IJump ijump;

    [SerializeField] private Transform skeletionAnimaiton;

    private void Start()
    {
        ijump = GetComponent<IJump>();
        body = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        move.x = Input.GetAxisRaw("Horizontal");
        move.y = Input.GetAxisRaw("Vertical");
        CheckFlip();
        CheckPlayAnim();
    }

    private void FixedUpdate()
    {
        this.body.position += move * (runSpeed * Time.deltaTime);
    }

    private void CheckPlayAnim()
    {
        if (!ijump.jumping)
        {
            if (move.sqrMagnitude > 0f)
            {
                moving = true;
            }
            else
            {
                moving = false;
            }
            // play anim
        }
    }

    private void CheckFlip()
    {
        if (move.x > 0f && !m_FacingRight)
        {
            Flip();
        }
        else if (move.x < 0 && m_FacingRight)
        {
            Flip();
        }
    }

    private void Flip()
    {
        // Switch the way the player is labelled as facing.
        m_FacingRight = !m_FacingRight;
        // Multiply the player's x local scale by -1.
        var transform1 = this.skeletionAnimaiton.transform;
        Vector3 theScale = transform1.localScale;
        theScale.x *= -1;
        transform1.localScale = theScale;
    }
}