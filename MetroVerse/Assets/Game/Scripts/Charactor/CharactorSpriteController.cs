﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharactorSpriteController : MonoBehaviour
{
    public ESKINCHARACTORCPN Eskincharactorcpn;

    private SpriteRenderer spriteRenderer;

    private SpriteRenderer SpriteRenderer
    {
        get
        {
            if (this.spriteRenderer == null)
            {
                this.spriteRenderer = this.GetComponent<SpriteRenderer>();
            }

            return this.spriteRenderer;
        }
    }

    public void ChangeSprite(Sprite sprite)
    {
        this.SpriteRenderer.sprite = sprite;
    }

    public void SetOrderID(int value)
    {
        this.SpriteRenderer.sortingOrder = value;
    }

    public void IsOnSprite(bool isOn)
    {
        this.SpriteRenderer.enabled = isOn;
    }
}