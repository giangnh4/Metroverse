﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class CharactorJump : MonoBehaviour, IJump
{
    private float timeJumpValue;

    [SerializeField] private float yLocalValue = 2f;

    [SerializeField] private Transform skeleton;


    private float ySaveValue = 0;


    public float jumpValue
    {
        get => this.timeJumpValue;
        set => timeJumpValue = value;
    }

    public bool jumping { get; set; }
    public bool jumped { get; set; }

    private void Start()
    {
        this.ySaveValue = this.skeleton.transform.localPosition.y;
    }

    [EditorButton]
    public void OnJump()
    {
        if (jumping)
        {
            return;
        }

        jumping = true;
        //this._playerAnimator.JumpAnimation(jumping);
        Debug.Log("jump");
        this.skeleton.transform.DOLocalMoveY(yLocalValue, timeJumpValue).SetLoops(2, LoopType.Yoyo).OnComplete(
            delegate
            {
                jumping = false;
                // this._playerAnimator.JumpAnimation(jumping);
                Vector3 pos = new Vector3(this.skeleton.transform.localPosition.x,
                    this.ySaveValue, this.skeleton.transform.localPosition.z);
                this.skeleton.transform.localPosition = pos;
            });
    }
}