﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Spine;
using Spine.Unity;
using UnityEngine;
using Event = Spine.Event;

public class CharactorSkeletonAnimationController : MonoBehaviour
{
    private SkeletonAnimation _skeletonAnimation;

    private SkeletonAnimation skeletonAnimation
    {
        get
        {
            if (this._skeletonAnimation == null)
            {
                this._skeletonAnimation = this.GetComponent<SkeletonAnimation>();
            }

            return this._skeletonAnimation;
        }
    }

    [SpineAnimation] private string AnimationHi = "Hi";
    [SpineAnimation] private string AnimationIdle = "Idle";
    [SpineAnimation] private string AnimationJump = "Jump";
    [SpineAnimation] private string AnimationWalk = "Walk";
    private bool inited = false;
    private Spine.AnimationState _animationState;

    private int trackIndex = 1;

    void Start()
    {
        this._animationState = this.skeletonAnimation.AnimationState;
        // registering for events raised by any animation
        this._animationState.Start += OnSpineAnimationStart;
        this._animationState.Interrupt += OnSpineAnimationInterrupt;
        this._animationState.End += OnSpineAnimationEnd;
        this._animationState.Dispose += OnSpineAnimationDispose;
        this._animationState.Complete += OnSpineAnimationComplete;

        this._animationState.Event += OnUserDefinedEvent;
    }

    private void OnUserDefinedEvent(TrackEntry trackentry, Event e)
    {
    }

    private void OnSpineAnimationComplete(TrackEntry trackentry)
    {
    }

    private void OnSpineAnimationDispose(TrackEntry trackentry)
    {
    }

    private void OnSpineAnimationEnd(TrackEntry trackentry)
    {
    }

    private void OnSpineAnimationInterrupt(TrackEntry trackentry)
    {
    }

    private void OnSpineAnimationStart(TrackEntry trackentry)
    {
    }

    public void PlayAnim(string anim, bool isLoop)
    {
        // registering for events raised by a single animation track entry
        this._animationState = this.skeletonAnimation.AnimationState;
        Spine.TrackEntry trackEntry = this._animationState.SetAnimation(trackIndex, anim, isLoop);
        trackEntry.Start += OnSpineAnimationStart;
        trackEntry.Interrupt += OnSpineAnimationInterrupt;
        trackEntry.End += OnSpineAnimationEnd;
        trackEntry.Dispose += OnSpineAnimationDispose;
        trackEntry.Complete += OnSpineAnimationComplete;
        trackEntry.Event += OnUserDefinedEvent;
    }

    [Button]
    public void PlayHi()
    {
        this.PlayAnim(AnimationHi, false);
    }

    [Button]
    public void PlayIdle()
    {
        this.PlayAnim(AnimationIdle, true);
    }

    [Button]
    public void PlayWalk()
    {
        this.PlayAnim(AnimationWalk, true);
    }

    [Button]
    public void PlayJump()
    {
        this.PlayAnim(AnimationJump, false);
    }
}