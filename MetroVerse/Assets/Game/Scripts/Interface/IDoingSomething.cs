﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDoingSomething
{
    bool DoingSomething { get; set; }
}