﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IJump
{
    float jumpValue { get; set; }
    bool jumping { get; set; }

    bool jumped { get; set; }

    void OnJump();
}